<?php

    do
    {
        echo "Digite a base do Retângulo";
        $b = trim(fgets(STDIN));
    }
    while (empty($b) || !is_numeric($b) || $b < 0);

    do
    {
        echo "Digite a altura do Retângulo";
        $a = trim(fgets(STDIN));
    }
    while (empty($a) || !is_numeric($a) || $a < 0);

    $r = $b * $a;

    echo "$r é o valor da área do Retângulo";
 ?>
