<?php

do
{
    echo "Digite o valor da aresta do quadrado";
    $a = trim(fgets(STDIN));
}
while (empty($a) || !is_numeric($a) || $a < 0);

$r = $a * $a;

echo "$r é o valor da área do quadrado.";

?>
