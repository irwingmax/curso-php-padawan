<?php

// Calcular e exibir a área de um quadrado, a partir do valor de sua aresta que será digitado.
$aresta;
$area;

echo "\nDigite o valor da aresta: ";
$aresta = trim(fgets(STDIN));

$area = $aresta*$aresta;

echo "\nA área é: $area";