<?php

// Entrar via teclado com o valor de uma temperatura em graus Celsius, calcular e exibir sua temperatura
// equivalente em Fahrenheit.
$celsius;
$farenheit;

echo "\nDigite a temperatura em Celsius: ";
$celsius = trim(fgets(STDIN));

$farenheit = $celsius * 1.8 + 32;

echo "\nA temperatura em Fahrenheit é: $farenheit";