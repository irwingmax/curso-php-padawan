<?php


// Entrar via teclado com a base e a altura de um retângulo, calcular e exibir sua área
$base;
$altura;
$area;

echo "\nDigite a base do retângulo: ";
$base = trim(fgets(STDIN));

echo "\nDigite a altura do retângulo: ";
$altura = trim(fgets(STDIN));

$area = $base*$altura;
echo "A área é: $area";