<?php 
$msgNegativa = 'Você ficou devendo: R$';
$msgTroco = 'Pagamento correto, seu troco será de: R$';
$msgSemTroco = 'Pagamento correto, sem troco';
$soma = 0;
for ($i = 0; $i < 5;$i++){
	$produto = trim(fgets(STDIN));
	$soma += $produto;
}

$pagamento = trim(fgets(STDIN));

$compra = $pagamento - $soma;

if($compra < 0){
	echo $msgNegativa . $compra;
}

if($compra == 0){
	echo $msgSemTroco;
}
if($compra > 0){
	echo $msgTroco . $compra;
}

?>