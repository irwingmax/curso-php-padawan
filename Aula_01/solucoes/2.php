<?php

// Calcular e exibir a área de um quadrado, a partir do valor de sua aresta que será digitado.

$aresta = trim(fgets(STDIN));

/**
 * pow funcao que calcula potencia
 * @param 1 Base
 * @param 2 Expoente
 */
$area = pow($aresta,2);

echo $area;
echo "\n";