<?php

// Calcular e exibir a área de um quadrado a partir do valor de sua diagonal que será digitado.

$diagonal = trim(fgets(STDIN));

$area = pow($diagonal,2) / 2;

echo $area."\n";