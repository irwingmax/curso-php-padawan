<?php
class CalculadoraFiguras {

    private $figura;


    public function calculoArea(){
        return $this->figura->calcularArea();
    }


    /**
     * Gets the value of figura.
     *
     * @return mixed
     */
    public function getFigura()
    {
        return $this->figura;
    }

    /**
     * Sets the value of figura.
     *
     * @param mixed $figura the figura
     *
     * @return self
     */
    public function setFigura(Figura $figura)
    {
        $this->figura = $figura;

        return $this;
    }
}