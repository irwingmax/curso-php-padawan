<?php

class Circulo implements Figura {
    private $raio;

    public function calcularArea() {
        return 2 * 3.14 * $this->raio;
    }


    /**
     * Gets the value of raio.
     *
     * @return mixed
     */
    public function getRaio()
    {
        return $this->raio;
    }

    /**
     * Sets the value of raio.
     *
     * @param mixed $raio the raio
     *
     * @return self
     */
    public function setRaio($raio)
    {
        if($raio <= 0){
            throw new FiguraException('Erro no círculo malandrão');
        }
        $this->raio = $raio;

        return $this;
    }
}