<?php	

class Banco 
{
	private $Conexao;

    /**
     * Gets the value of Conexao.
     *
     * @return mixed
     */
    public function getConexao()
    {
        return $this->Conexao;
    }

    /**
     * Sets the value of Conexao.
     *
     * @param mixed $Conexao the p d o
     *
     * @return self
     */
    public function setConexao($Conexao)
    {
        $this->Conexao = $Conexao;

        return $this;
    }
    public function Conectar()
    {
        $this->Conexao = new PDO('mysql:host=127.0.0.1;dbname=curso', "root", "");

        if($this->Conexao == null){
            return false;
        } else {
            return $this->Conexao;
        }
    }
}
?>