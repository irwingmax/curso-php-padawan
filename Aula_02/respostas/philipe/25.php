<?php

/*
	Entrar com o peso e a altura de uma determinada pessoa. Após a digitação, exibir se esta pessoa está ou não com seu peso ideal. Veja tabela da relação peso/altura².

	TABELA
	R < 20       | Abaixo do peso
	20 <= R < 25 | Peso ideal
	R >= 25      | Acima do peso

 */